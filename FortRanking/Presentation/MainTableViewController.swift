//
//  MainTableViewController.swift
//
// Copyright (c) 21/12/15. Ramotion Inc. (http://ramotion.com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import UIKit
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD
import Persei

class MainTableViewController: UITableViewController ,ForniteManagerDelegate{
  
    
    internal let refreshC = UIRefreshControl()
    fileprivate var menu: MenuView!
    let kCloseCellHeight: CGFloat = 179
    let kOpenCellHeight: CGFloat = 488
    var kRowsCount = 0
    var cellHeights: [CGFloat] = []
    var usuarios  = [UserResponse]()
    var filtroMenu: Int = 0
   var manager : ForniteManager?
  
    // MARK: - Items
    fileprivate let items = (0..<3).map {
        MenuItem(image: UIImage(named: "menu_icon_\($0)")!)
    }

    // MARK: - View

    fileprivate func loadMenu() {
        menu = {
            let menu = MenuView()
            menu.items = items
            menu.delegate = self


            return menu
        }()
        
        tableView.addSubview(menu)
    }
    fileprivate func removeMenu() {
    
        
        menu.removeFromSuperview()
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        loadMenu()
        kRowsCount = ForniteManager.getNumberPlayersPlist()
        let button = UIButton.init(type: .custom)
        //set image for button
        button.setImage(UIImage(named: "filter"), for: UIControlState.normal)
        //add function for button
        button.addTarget(self, action: #selector(filter), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 53, height: 51)

        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = barButton


        self.navigationItem.title = "FortRanking"
        manager = ForniteManager()
        manager?.delegate = self
        manager?.reload(filtro: 0)
        setup()
        SVProgressHUD.show()
        
    }
    

    
    private func setup() {
        cellHeights = Array(repeating: kCloseCellHeight, count: kRowsCount)
        tableView.estimatedRowHeight = kCloseCellHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        // tableView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
        let imageV : UIImageView = UIImageView(image: UIImage(named: "background"))
        imageV.contentMode = .scaleToFill
        tableView.backgroundView = imageV
        
        if #available(iOS 10.0, *) {
          // tableView.refreshControl = self.refreshC
           // tableView.refreshControl?.addTarget(self, action: #selector(reload), for: .valueChanged)
        }
 
        
    }
    
    @objc private func reload() {
        
        self.manager?.reload(filtro: filtroMenu)
     //   menu.removeFromSuperview()
        
    }
    
    @objc private func filter() {
        
        // or animated
        menu.setRevealed(true, animated: true)
//
    }
    
    
    // MARK: - ForniteManagerDelegate
    func refresh(usuarios: [UserResponse]) {
        self.tableView.refreshControl?.endRefreshing()
        self.usuarios = usuarios
        self.tableView.reloadData()
    }
   
}

// MARK: - TableView

// MARK: - MenuViewDelegate
extension MainTableViewController: MenuViewDelegate {
    
    public func menu(_ menu: MenuView, didSelectItemAt index: Int) {
        //menu.delegate = nil
        filtroMenu = index
        reload()
      
    }
}

extension MainTableViewController {
    
    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.usuarios.count
    }
    
    override func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as DemoCell = cell else {
            return
        }
        
        cell.backgroundColor = .clear
        
        if cellHeights[indexPath.row] == kCloseCellHeight {
            cell.unfold(false, animated: false, completion: nil)
        } else {
            cell.unfold(true, animated: false, completion: nil)
        }
        
        cell.number = indexPath.row
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoldingCell", for: indexPath) as! DemoCell
        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
        cell.jugador = self.usuarios[indexPath.row]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations
        return cell
    }
    
    override func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! DemoCell
        
        if cell.isAnimating() {
            return
        }
        
        var duration = 0.0
        let cellIsCollapsed = cellHeights[indexPath.row] == kCloseCellHeight
        if cellIsCollapsed {
            cellHeights[indexPath.row] = kOpenCellHeight
            cell.unfold(true, animated: true, completion: nil)
            duration = 0.5
        } else {
            cellHeights[indexPath.row] = kCloseCellHeight
            cell.unfold(false, animated: true, completion: nil)
            duration = 0.8
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
            tableView.beginUpdates()
            tableView.endUpdates()
        }, completion: nil)
    }
    
    
   
}




