//
//  DemoCell.swift
//  FoldingCell
//
//  Created by Alex K. on 25/12/15.
//  Copyright © 2015 Alex K. All rights reserved.
//

import UIKit

class DemoCell: FoldingCell {
    @IBOutlet weak var titleWins: UILabel!
    @IBOutlet weak var titlePuntos: UILabel!
    
    @IBOutlet weak var goldImage: UIImageView!
    @IBOutlet weak var puntosLabel: UILabel!
    @IBOutlet weak var winsLabel: UILabel!
    @IBOutlet weak var titleKills: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet var closeNumberLabel: UILabel!
    @IBOutlet weak var killsLabel: UILabel!
    @IBOutlet var openNumberLabel: UILabel!
    var  jugador : UserResponse!
    var number: Int = 0 {
        didSet {
            closeNumberLabel.text = String(number+1)
            openNumberLabel.text = String(number+1)
            nameLabel.text = jugador.epicUserHandle
            titleKills.text = "Kills"
            titleWins.text = "Wins"
            titlePuntos.text = "Puntos"

            puntosLabel.text = String(jugador.lifeTimeScore ?? 0)
            winsLabel.text =  String(jugador.lifeTimeWins ?? 0)
            killsLabel.text =  String(jugador.lifeTimeKills ?? 0)

          // por defecto
                let colorPurple = UIColor(hex: "5D4A99")
                goldImage.isHidden = true
                
                leftView.backgroundColor = colorPurple
                
          
                /// GANADOR
            
            if (number == 0)
            {
               let colorGolden = UIColor(hex: "FEBE16")
                goldImage.isHidden = false
                leftView.backgroundColor = colorGolden
            }
               // PLATA
            if (number == 1)
            {
                let colorGolden = UIColor(hex: "D3D3D3")
                goldImage.isHidden = false
                goldImage.image = UIImage (named: "second")
                leftView.backgroundColor = colorGolden
            }
            
            // BRONCe
            if (number == 2)
            {
                let colorGolden = UIColor(hex: "cd7f32")
                goldImage.isHidden = false
                goldImage.image = UIImage (named: "third")

                leftView.backgroundColor = colorGolden
            }
                
           
            
        }
        
    }

    override func awakeFromNib() {
        foregroundView.layer.cornerRadius = 10
        foregroundView.layer.masksToBounds = true
        super.awakeFromNib()
    }

    override func animationDuration(_ itemIndex: NSInteger, type _: FoldingCell.AnimationType) -> TimeInterval {
        let durations = [0.26, 0.2, 0.2]
        return durations[itemIndex]
    }
}

// MARK: - Actions ⚡️

extension DemoCell {

    @IBAction func buttonHandler(_: AnyObject) {
        print("tap")
    }
}
