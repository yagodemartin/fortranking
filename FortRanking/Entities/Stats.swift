/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar



import Foundation
struct Stats : Codable {
	let p2 : P2?
	let p10 : P10?
	let p9 : P9?
	let curr_p2 : Curr_p2?
	let curr_p10 : Curr_p10?
	let curr_p9 : Curr_p9?

	enum CodingKeys: String, CodingKey {

		case p2
		case p10
		case p9
		case curr_p2
		case curr_p10
		case curr_p9
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		p2 = try P2(from: decoder)
		p10 = try P10(from: decoder)
		p9 = try P9(from: decoder)
		curr_p2 = try Curr_p2(from: decoder)
		curr_p10 = try Curr_p10(from: decoder)
		curr_p9 = try Curr_p9(from: decoder)
	}

}
*/
