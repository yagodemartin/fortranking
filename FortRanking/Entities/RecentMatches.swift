/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct RecentMatches : Codable {
	let id : Int?
	let accountId : String?
	let playlist : String?
	let kills : Int?
	let minutesPlayed : Int?
	let top1 : Int?
	let top5 : Int?
	let top6 : Int?
	let top10 : Int?
	let top12 : Int?
	let top25 : Int?
	let matches : Int?
	let top3 : Int?
	let dateCollected : String?
	let score : Int?
	let platform : Int?
	let trnRating : Double?
	let trnRatingChange : Double?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case accountId = "accountId"
		case playlist = "playlist"
		case kills = "kills"
		case minutesPlayed = "minutesPlayed"
		case top1 = "top1"
		case top5 = "top5"
		case top6 = "top6"
		case top10 = "top10"
		case top12 = "top12"
		case top25 = "top25"
		case matches = "matches"
		case top3 = "top3"
		case dateCollected = "dateCollected"
		case score = "score"
		case platform = "platform"
		case trnRating = "trnRating"
		case trnRatingChange = "trnRatingChange"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		accountId = try values.decodeIfPresent(String.self, forKey: .accountId)
		playlist = try values.decodeIfPresent(String.self, forKey: .playlist)
		kills = try values.decodeIfPresent(Int.self, forKey: .kills)
		minutesPlayed = try values.decodeIfPresent(Int.self, forKey: .minutesPlayed)
		top1 = try values.decodeIfPresent(Int.self, forKey: .top1)
		top5 = try values.decodeIfPresent(Int.self, forKey: .top5)
		top6 = try values.decodeIfPresent(Int.self, forKey: .top6)
		top10 = try values.decodeIfPresent(Int.self, forKey: .top10)
		top12 = try values.decodeIfPresent(Int.self, forKey: .top12)
		top25 = try values.decodeIfPresent(Int.self, forKey: .top25)
		matches = try values.decodeIfPresent(Int.self, forKey: .matches)
		top3 = try values.decodeIfPresent(Int.self, forKey: .top3)
		dateCollected = try values.decodeIfPresent(String.self, forKey: .dateCollected)
		score = try values.decodeIfPresent(Int.self, forKey: .score)
		platform = try values.decodeIfPresent(Int.self, forKey: .platform)
		trnRating = try values.decodeIfPresent(Double.self, forKey: .trnRating)
		trnRatingChange = try values.decodeIfPresent(Double.self, forKey: .trnRatingChange)
	}

}