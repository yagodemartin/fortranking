//
//  ListadoColeguis.swift
//  FortRanking
//
//  Created by Yago de Martin Lopez on 29/5/18.
//  Copyright © 2018 Yago de Martin Lopez. All rights reserved.
//

import UIKit

struct ListadoColeguis: Codable {
    var PS4: [String]?
    var PC: [String]?
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        PS4 = try values.decodeIfPresent([String].self, forKey: .PS4)
        PC = try values.decodeIfPresent([String].self, forKey: .PC)

    }
    
}
