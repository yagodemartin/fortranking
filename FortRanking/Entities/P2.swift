/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar



import Foundation
struct P2 : Codable {
	let trnRating : TrnRating?
	let score : Score?
	let top1 : Top1?
	let top3 : Top3?
	let top5 : Top5?
	let top6 : Top6?
	let top10 : Top10?
	let top12 : Top12?
	let top25 : Top25?
	let kd : Kd?
	let winRatio : WinRatio?
	let matches : Matches?
	let kills : Kills?
	let kpg : Kpg?
	let scorePerMatch : ScorePerMatch?

	enum CodingKeys: String, CodingKey {

		case trnRating
		case score
		case top1
		case top3
		case top5
		case top6
		case top10
		case top12
		case top25
		case kd
		case winRatio
		case matches
		case kills
		case kpg
		case scorePerMatch
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		//trnRating = try TrnRating(from: decoder)
		score = try Score(from: decoder)
		top1 = try Top1(from: decoder)
		top3 = try Top3(from: decoder)
		top5 = try Top5(from: decoder)
		top6 = try Top6(from: decoder)
		top10 = try Top10(from: decoder)
		top12 = try Top12(from: decoder)
		top25 = try Top25(from: decoder)
		kd = try Kd(from: decoder)
		winRatio = try WinRatio(from: decoder)
		matches = try Matches(from: decoder)
		kills = try Kills(from: decoder)
		kpg = try Kpg(from: decoder)
		scorePerMatch = try ScorePerMatch(from: decoder)
	}

}
*/
