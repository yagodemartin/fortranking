//
//  ForniteManager.swift
//  FortRanking
//
//  Created by Yago de Martin Lopez on 8/6/18.
//  Copyright © 2018 Yago de Martin Lopez. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD


protocol ForniteManagerDelegate: class {
    func refresh(usuarios:[UserResponse])
}

class ForniteManager :NSObject {
    
    class func endpointForSpecies() -> String {
        return "https://swapi.co/api/species/"
    }
    
    var headers:  HTTPHeaders = [
        "TRN-Api-Key": "ffdd04d5-57fc-497c-b43d-8f7f1c8403a9"
        
    ]
    var usuarios  = [UserResponse]()
    let myGroup = DispatchGroup()

    var nombrecitosPS4 = [String]()
    var nombrecitosPC = [String]()
    
    var delegate : ForniteManagerDelegate?
    
    @objc  func reload(filtro: Int) {
        
    
        // location of plist file
        if let settingsURL = Bundle.main.path(forResource: "Nombrecitos", ofType: "plist") {
            
            do {
                var settings: ListadoColeguis?
                let data = try Data(contentsOf: URL(fileURLWithPath: settingsURL))
                let decoder = PropertyListDecoder()
                settings = try decoder.decode(ListadoColeguis.self, from: data)
                print("array  is \(settings?.PS4 ?? [""])")//prints array  is ["Good morning", "Good afternoon"]
                nombrecitosPS4 = (settings?.PS4)!
                nombrecitosPC = (settings?.PC)!
                
            } catch {
                print(error)
            }
        }
        
        
        
        var usuarios2 = [UserResponse]()
        if (nombrecitosPS4.count > 0)
            
        {
            for nombre in nombrecitosPS4 {
                
                myGroup.enter()
                
                Alamofire.request("https://api.fortnitetracker.com/v1/profile/ps4/"+nombre, headers: headers).responseObject { (response: DataResponse<UserResponse>) in
                    
                    let userResponse = response.result.value
                    if ((userResponse) != nil){
                    print(userResponse?.epicUserHandle ?? "")
                    userResponse?.setValues()
                    usuarios2.append(userResponse!)
                    }
                    self.myGroup.leave()
                    
                }
                
                
                
            }
            
            for nombre in nombrecitosPC {
                
                myGroup.enter()
                
                Alamofire.request("https://api.fortnitetracker.com/v1/profile/pc/"+nombre, headers: headers).responseObject { (response: DataResponse<UserResponse>) in
                    
                    let userResponse = response.result.value
                    if ((userResponse) != nil){
                        print(userResponse?.epicUserHandle ?? "")
                        userResponse?.setValues()
                        usuarios2.append(userResponse!)
                    }
                    self.myGroup.leave()
                    
                }
                
                
                
            }
            
            
            
            myGroup.notify(queue: DispatchQueue.main, execute: {
                self.usuarios = usuarios2
                //self.tableView.refreshControl?.endRefreshing()
               self.filtrado(filtro: filtro)
               
                
            })
            
        }
        
        
    }
    
    @objc  func filtrado(filtro: Int) {
        
        let usuarios2 = self.usuarios

        if (filtro == 0)
        {
            let sortedFriends = usuarios2.sorted(by: { $0.lifeTimeScore ?? 0 > $1.lifeTimeScore ?? 0 })
            self.usuarios = sortedFriends
            
        }
        
        if (filtro == 1)
        {
            let sortedFriends = usuarios2.sorted(by: { $0.lifeTimeKills ?? 0 > $1.lifeTimeKills ?? 0 })
            self.usuarios = sortedFriends
            
        }
        if (filtro == 2)
        {
            let sortedFriends = usuarios2.sorted(by: { $0.lifeTimeWins ?? 0 > $1.lifeTimeWins ?? 0 })
            self.usuarios = sortedFriends
            
        }
        
        
        // self.tableView.reloadData()
        self.delegate?.refresh(usuarios: self.usuarios)
        SVProgressHUD.dismiss()
    }

    static func getNumberPlayersPlist() -> Int {
        var dictionaryObj: NSDictionary?
        var allUsers = 1
        if let filePath = Bundle.main.path(forResource: "Nombrecitos", ofType: "plist")
        {
            dictionaryObj = NSDictionary(contentsOfFile: filePath)
        }
        if dictionaryObj != nil
        {
            //parse it as NSDictionary
            var users : [Any]?
            for index in (dictionaryObj?.allValues)!{
                
                users = index as? [Any]
                allUsers = (users?.count)! + allUsers
            }
            
        }
        else
        {
            //dictionaryObj is nil or doesn't contain anything.
            return allUsers
        }
        return allUsers
    }
    

}
